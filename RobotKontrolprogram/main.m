//
//  main.m
//  RobotKontrolprogram
//
//  Created by Mathias Hansen on 30/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
