//
//  RobotKPView.m
//  RobotKontrolprogram
//
//  Created by Mathias Hansen on 30/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "RobotKPView.h"
#import "Masonry.h"

// Constants
float const CELL_SIZE = 20.0f;
float const ROBOT_SIZE = 12.0f;
float const SOME_VIEWS_HEIGHT = 40.0f;

// Variables
float offGridToBeInsideCell_x = 0.0f;
float offGridToBeInsideCell_y = 0.0f;

@interface RobotKPView () <UITextFieldDelegate>  // we want to listen for keyboard input
@end

@implementation RobotKPView
{
	UITextField *roomSize, *startPositionInput, *navigationCommands;
	UIView *inputViewContainer, *outputViewContainer;
	UIImageView *robot;
	BOOL startPosition, moving, turning, toggleOfOnGrid;
	NSMutableArray *startPositionArray;
	NSArray *roomSizeArray;
	UILabel *startPositionInputMessage, *roomSizeMessage, *navigationCommandsMessage, *robotReport;
	UIButton *reportBtn;
	UISegmentedControl *offGridOrOnGridSC;
}


// todo delete this in production
- (void)injected
{
	NSLog(@"I've been injected: %@", self);
	[self setupViews];
	
}

#pragma mark - Setup Views

- (id)init {
	self = [super init];
	if (!self) return nil;
	
	[self setupViews];
	
	return self;
}

-(void)setupViews {
	
	UIView *superview = self;
	int padding = 10;
	
	// todo use stackview as a container instead and more autolayout
	inputViewContainer = UIView.new;
	inputViewContainer.backgroundColor = UIColor.yellowColor;
	inputViewContainer.layer.borderColor = UIColor.blackColor.CGColor;
	inputViewContainer.layer.borderWidth = 2;
	[superview addSubview:inputViewContainer];
	
	outputViewContainer = UIView.new;
	outputViewContainer.backgroundColor = UIColor.whiteColor;
	outputViewContainer.layer.borderColor = UIColor.blackColor.CGColor;
	outputViewContainer.layer.borderWidth = 2;
	[superview addSubview:outputViewContainer];
	
	[inputViewContainer mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.greaterThanOrEqualTo(superview.mas_top).offset(padding);
		make.left.equalTo(superview.mas_left).offset(padding);
		make.bottom.equalTo(superview.mas_bottom).offset(-padding);
		make.right.equalTo(outputViewContainer.mas_left).offset(-padding);
		make.width.equalTo(outputViewContainer.mas_width);
		
	}];
	
	//with is semantic and option
	[outputViewContainer mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(superview.mas_top).with.offset(padding); //with with
		make.left.equalTo(inputViewContainer.mas_right).offset(padding); //without with
		make.bottom.equalTo(superview.mas_bottom).offset(-padding);
		make.right.equalTo(superview.mas_right).offset(-padding);
		make.width.equalTo(inputViewContainer.mas_width);
		
		make.height.equalTo(@[inputViewContainer]); //can pass array of views
	}];
	/* inputput container */
	roomSize = [[UITextField alloc] initWithFrame:CGRectMake(padding, padding, 300, 40)];
	roomSize.placeholder = @"Rum størrelse. Fx 5 7 (5 kolonner og 7 rækker)";
	roomSize.backgroundColor = [UIColor whiteColor];
	roomSize.tag = 1;
	roomSize.layer.borderColor = UIColor.blackColor.CGColor;
	roomSize.layer.borderWidth = 1;
	// we want the view to know when the user enters a grid size
	roomSize.delegate = self;
	roomSize.autocorrectionType = UITextAutocorrectionTypeNo;
	[inputViewContainer addSubview:roomSize];
	roomSizeMessage = [[UILabel alloc] initWithFrame:CGRectMake(padding, 50, 300, 10)];
	roomSizeMessage.hidden = YES;
	roomSizeMessage.text = @"Forkert input";
	roomSizeMessage.font=[roomSizeMessage.font fontWithSize:10];
	[inputViewContainer addSubview:roomSizeMessage];
	[roomSize mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(inputViewContainer.mas_top).offset(padding);
		make.left.equalTo(inputViewContainer.mas_left).offset(padding); 
		make.right.equalTo(inputViewContainer.mas_right).offset(-padding);
		make.height.equalTo(@(SOME_VIEWS_HEIGHT));
	}];
	
	startPositionInput = [[UITextField alloc] initWithFrame:CGRectMake(padding, 70, 300, 40)];
	startPositionInput.placeholder = @"Startposition. Fx 1 2 N";
	startPositionInput.backgroundColor = [UIColor whiteColor];
	startPositionInput.layer.borderColor = UIColor.blackColor.CGColor;
	startPositionInput.layer.borderWidth = 1;
	// we want the view to know when the user enters a command to the robot
	startPositionInput.delegate = self;
	startPositionInput.autocorrectionType = UITextAutocorrectionTypeNo;
	startPositionInput.tag = 2;
	[inputViewContainer addSubview:startPositionInput];
	startPositionInputMessage = [[UILabel alloc] initWithFrame:CGRectMake(padding, 110, 300, 10)];
	startPositionInputMessage.hidden = YES;
	startPositionInputMessage.text = @"Forkert input";
	startPositionInputMessage.font =[startPositionInputMessage.font fontWithSize:10];
	[inputViewContainer addSubview:startPositionInputMessage];
	[startPositionInput mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(roomSizeMessage.mas_bottom).offset(padding);
		make.left.equalTo(inputViewContainer.mas_left).offset(padding);
		make.right.equalTo(inputViewContainer.mas_right).offset(-padding);
		make.height.equalTo(@(SOME_VIEWS_HEIGHT));
	}];
	
	navigationCommands = [[UITextField alloc] initWithFrame:CGRectMake(padding, 130, 300, 40)];
	navigationCommands.placeholder = @"navigationskommandoer. Fx RFRFFRFRF";
	navigationCommands.backgroundColor = [UIColor whiteColor];
	navigationCommands.tag = 3;
	navigationCommands.layer.borderColor = UIColor.blackColor.CGColor;
	navigationCommands.layer.borderWidth = 1;
	// we want the view to know when the user enters a command to the robot
	navigationCommands.delegate = self;
	navigationCommands.autocorrectionType = UITextAutocorrectionTypeNo;
	[inputViewContainer addSubview:navigationCommands];
	navigationCommandsMessage = [[UILabel alloc] initWithFrame:CGRectMake(padding, 170, 300, 10)];
	navigationCommandsMessage.hidden = YES;
	navigationCommandsMessage.text = @"Forkert input";
	navigationCommandsMessage.font=[navigationCommandsMessage.font fontWithSize:10];
	[inputViewContainer addSubview:navigationCommandsMessage];
	[navigationCommands mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(startPositionInputMessage.mas_bottom).offset(padding);
		make.left.equalTo(inputViewContainer.mas_left).offset(padding);
		make.right.equalTo(inputViewContainer.mas_right).offset(-padding);
		make.height.equalTo(@(SOME_VIEWS_HEIGHT));
	}];
	
	robotReport = [[UILabel alloc] initWithFrame:CGRectMake(padding, 170, 300, 40)];
	robotReport.hidden = YES;
	[inputViewContainer addSubview:robotReport];
	
	UIButton *resetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[resetBtn addTarget:self action:@selector(reset)
	   forControlEvents:UIControlEventTouchUpInside];
	[resetBtn setTitle:@"Forfra" forState:UIControlStateNormal];
	resetBtn.frame = CGRectMake(padding, 220.0, 80.0, 40.0);
	resetBtn.backgroundColor = [UIColor redColor];
	resetBtn.layer.cornerRadius = 10;
	resetBtn.clipsToBounds = YES;
	resetBtn.layer.borderColor = UIColor.blackColor.CGColor;
	resetBtn.layer.borderWidth = 1;
	[inputViewContainer addSubview:resetBtn];
	[inputViewContainer addSubview:reportBtn];
	
	reportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[reportBtn addTarget:self action:@selector(report)
		forControlEvents:UIControlEventTouchUpInside];
	[reportBtn setTitle:@"Rapport" forState:UIControlStateNormal];
	reportBtn.frame = CGRectMake(200.0, 220.0, 80.0, 40.0);
	reportBtn.backgroundColor = [UIColor blueColor];
	reportBtn.layer.cornerRadius = 10;
	reportBtn.clipsToBounds = YES;
	reportBtn.layer.borderColor = UIColor.blackColor.CGColor;
	reportBtn.layer.borderWidth = 1;
	reportBtn.hidden = YES;
	[inputViewContainer addSubview:reportBtn];
	
	[resetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(robotReport.mas_bottom);
		make.left.equalTo(inputViewContainer.mas_left).offset(padding);
		make.right.equalTo(reportBtn.mas_left).offset(-padding-padding);
		make.height.equalTo(@(SOME_VIEWS_HEIGHT));
		make.width.equalTo(reportBtn.mas_width);
	}];
	[reportBtn mas_makeConstraints:^(MASConstraintMaker *make) {
		make.top.equalTo(robotReport.mas_bottom);
		make.left.equalTo(resetBtn.mas_right).offset(padding+padding);
		make.right.equalTo(inputViewContainer.mas_right).offset(-padding);
		make.height.equalTo(@(SOME_VIEWS_HEIGHT));
		
		make.width.equalTo(resetBtn.mas_width);
	}];

	
	offGridOrOnGridSC = [[UISegmentedControl alloc]initWithItems:@[@"På gitteret",@"I cellen"]];
	
	offGridOrOnGridSC.frame = CGRectMake(40, 270, 160, 20);
	[offGridOrOnGridSC addTarget:self action:@selector(offGridOrOnGridSCValueDidChange:) forControlEvents:UIControlEventValueChanged];
	[offGridOrOnGridSC setSelectedSegmentIndex:0];
	offGridOrOnGridSC.hidden = YES;
	[inputViewContainer addSubview:offGridOrOnGridSC];
	
	
	/* output container */
	robot = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 12, 12)];
	robot.hidden = YES;
	startPosition = NO;
	UIImage* image = [UIImage imageNamed:@"193-location-arrow.png"];
	robot.image = image;
	[outputViewContainer addSubview:robot];
	
}

#pragma mark - Button click handling

-(void)reset {

	reportBtn.hidden = YES;
	[self setupViews];
}

-(void)report {
	robotReport.hidden = NO;
	
	int x = (int) [startPositionArray[0] integerValue];
	int y = (int) [startPositionArray[1] integerValue];
	NSString *compassDirection = [startPositionArray[2] uppercaseString];
	NSLog(@"x : %d,\t y : %d,\t compass : %@", x, y, compassDirection);
	robotReport.text = [NSString stringWithFormat:@"Robotten rapporterer: %d %d %@", x, y, compassDirection];
}

-(void)offGridOrOnGrid:(int)selected
{
	int columns = (int) [roomSizeArray[0] integerValue];
	int rows = (int) [roomSizeArray[1] integerValue];
	
	BOOL isColumnsAEvenNumber = columns % 2 == 0;
	BOOL isRowsAEvenNumber = rows % 2 == 0;
	
	switch (selected) {
		case 0:			// on grid selected
		{
			if(isColumnsAEvenNumber && isRowsAEvenNumber)
			{
				offGridToBeInsideCell_x = 0.0f;
				offGridToBeInsideCell_y = 0.0f;
			}
			else if(!isColumnsAEvenNumber && !isRowsAEvenNumber)
			{
				offGridToBeInsideCell_x = 10.0f;
				offGridToBeInsideCell_y = 10.0f;
			}
			else if(isColumnsAEvenNumber && !isRowsAEvenNumber)
			{
				offGridToBeInsideCell_x = 0.0f;
				offGridToBeInsideCell_y = 10.0f;
			}
			else if(!isColumnsAEvenNumber && isRowsAEvenNumber)
			{
				offGridToBeInsideCell_x = 10.0f;
				offGridToBeInsideCell_y = 0.0f;
			}
			break;
		}
		case 1:			// off grid selected
		{			
			if(isColumnsAEvenNumber && isRowsAEvenNumber)
			{
				offGridToBeInsideCell_x = 10.0f;
				offGridToBeInsideCell_y = 10.0f;
			}
			else if(!isColumnsAEvenNumber && !isRowsAEvenNumber)
			{
				offGridToBeInsideCell_x = 0.0f;
				offGridToBeInsideCell_y = 0.0f;
			}
			else if(isColumnsAEvenNumber && !isRowsAEvenNumber)
			{
				offGridToBeInsideCell_x = 10.0f;
				offGridToBeInsideCell_y = 0.0f;
			}
			else if(!isColumnsAEvenNumber && isRowsAEvenNumber)
			{
				offGridToBeInsideCell_x = 0.0f;
				offGridToBeInsideCell_y = 10.0f;
			}
			break;
		}
	}
	
	toggleOfOnGrid = !toggleOfOnGrid;
	[self updatePosition];
}

-(void)offGridOrOnGridSCValueDidChange:(UISegmentedControl *)segment
{
	[self offGridOrOnGrid:segment.selectedSegmentIndex];
}

#pragma mark - UITextfield delegate methods

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
	if(textField.tag == 1) // grid size
	{
		roomSize.text = @""; // we don't want any old wrong inputs
		navigationCommandsMessage.hidden = YES; // we dont want this to be shown
		// When we start typing we don't want a wrong input message to be shown
		if(roomSizeMessage.hidden == NO)
			roomSizeMessage.hidden = YES;
		
		if(startPositionInputMessage.hidden == NO)
			startPositionInputMessage.hidden = YES;
	}
	else if(textField.tag == 2) // start position
	{
		navigationCommandsMessage.hidden = YES; // we dont want this to be shown
		if(roomSize.enabled == YES) // We must set room size first
		{
			startPositionInputMessage.text = @"Input felt Rum størrelse skal sættes først";
			startPositionInputMessage.hidden = NO;
			return;
		}
		startPositionInputMessage.hidden = YES;
		startPositionInput.text = @"";
	}
	else if(textField.tag == 3) // navigation commands
	{
		if(startPositionInput.enabled == YES)
		{
			navigationCommandsMessage.text = @"Input felt Startposition skal sættes først";
			navigationCommandsMessage.hidden = NO;
			return;
		}
	}
	
}


-(void)textFieldDidEndEditing:(UITextField *)textField {
	if(textField.tag == 1) // grid size
	{
		// todo move validation
		NSString *input = textField.text;
		roomSizeArray = [input componentsSeparatedByString:@" "];
	
		// todo we should validate input check if values are correct
		if(roomSizeArray.count < 2)
		{
			[self roomSizeTextfieldIsNotValid];
			return;
		}
		
		NSString *firstInputString = roomSizeArray[0];
		
		if([firstInputString isEqualToString:@""])
		{
			[self roomSizeTextfieldIsNotValid];
			return;
		}
		// todo validate is a number
//		int firstNo = [self isNumber:firstInputString];
//		if(firstNo isaN)
//		{
//			[self roomSizeTextfieldIsNotValid];
//			return;
//		}
		
		NSString *secondInputString = roomSizeArray[1];
		if([secondInputString isEqualToString:@""])
		{
			[self roomSizeTextfieldIsNotValid];
			return;
		}
		// todo validate is a number
//		int secondNo = = [self isNumber:firstInputString];
//		if(secondNo issN)
//		{
//			[self roomSizeTextfieldIsNotValid];
//			return;
//		}
		
		roomSize.enabled = NO;
		roomSize.textColor = [UIColor grayColor];
		[self drawGrid];
		startPositionInputMessage.text = @"Forkert input";
		
	}
	else if(textField.tag == 2) // start position
	{
		NSString *input2 = textField.text;
		startPositionArray = (NSMutableArray*) [input2 componentsSeparatedByString:@" "];
		NSString *firstElementSecondInputField = startPositionArray[0];
		// todo we should validate input check if values are correct
		
		if(startPositionArray.count < 3)
		{
			[self startPositionTextfieldIsNotValid];
			return;
		}
		
		if([firstElementSecondInputField isEqualToString:@""])
		{
			[self startPositionTextfieldIsNotValid];
			return;
		}
		// todo validate is a number
//		int firstNoSecondIputField =  [self isNumber:firstInputString];
//		if(firstNoSecondIputField == 0)
//		{
//			[self startPositionTextfieldIsNotValid];
//			return;
//		}
		NSString *secondElementSecondInputField = startPositionArray[1];
		// todo validate is a number
//		int secondNoSecondIputField =  [self isNumber:firstInputString];
//		if(secondNoSecondIputField  isaN)
//		{
//			[self startPositionTextfieldIsNotValid];
//			return;
//		}
		
		NSString * thirdElementSecondInputField = startPositionArray[2];
		if(![thirdElementSecondInputField isEqualToString:@"N"]
		   &&
		   ![thirdElementSecondInputField isEqualToString:@"S"]
		   &&
		   ![thirdElementSecondInputField isEqualToString:@"E"]
		   &&
		   ![thirdElementSecondInputField isEqualToString:@"W"])
		{
			[self startPositionTextfieldIsNotValid];
			return;
		}
		
		startPosition = YES;
		robot.hidden = NO;
		startPositionInput.enabled = NO;
		startPositionInput.textColor = [UIColor grayColor];
		[self updatePosition];
		
		// Now we have set a startposition so it makes sense to toggle on grid off grid
		offGridOrOnGridSC.hidden = NO;
		
	}

}

// Listen for inputs
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	if(textField.tag == 3) // navigation commands
	{
		
		// input validation
		if(![string isEqualToString:@"F"]
		   &&
		   ![string isEqualToString:@"L"]
		   &&
		   ![string isEqualToString:@"R"]
		   )
		{
			[self navigationCommandTextfieldIsNotValid];
			return NO;
		}
		
		// Input is now correct so don't show message
		navigationCommandsMessage.hidden = YES;
		
		// Now we have moved the robot show now it makes sense to show the report button
		reportBtn.hidden = NO;
		
		NSString *currentCompassDirection = [startPositionArray[2] uppercaseString];
		if([string.uppercaseString isEqualToString:@"F"]) // moving
		{
			if([currentCompassDirection isEqualToString:@"N"]) // move up
			{
				int y = (int) [startPositionArray[1] integerValue];
				y -= 1;
				startPositionArray[1] = [NSString stringWithFormat:@"%d", y];
			}
			else if([currentCompassDirection isEqualToString:@"W"]) // move left
			{
				int x = (int) [startPositionArray[0] integerValue];
				x -= 1;
				startPositionArray[0] = [NSString stringWithFormat:@"%d", x];
			}
			else if([currentCompassDirection isEqualToString:@"S"]) // move down
			{
				int y = (int) [startPositionArray[1] integerValue];
				y += 1;
				startPositionArray[1] = [NSString stringWithFormat:@"%d", y];
			}
			else if([currentCompassDirection isEqualToString:@"E"]) // move right
			{
				int x = (int) [startPositionArray[0] integerValue];
				x += 1;
				startPositionArray[0] = [NSString stringWithFormat:@"%d", x];
			}
			
			moving = YES;
			turning = NO;
		}
		else if([string.uppercaseString isEqualToString:@"L"]) // turning left. Not moving
		{
			moving = NO;
			turning = YES;
			if([currentCompassDirection isEqualToString:@"N"])
				startPositionArray[2] = @"W";
			else if([currentCompassDirection isEqualToString:@"W"])
				startPositionArray[2] = @"S";
			else if([currentCompassDirection isEqualToString:@"S"])
				startPositionArray[2] = @"E";
			else if([currentCompassDirection isEqualToString:@"E"])
				startPositionArray[2] = @"N";
		}
		else if([string.uppercaseString isEqualToString:@"R"]) // turning right. Not moving
		{
			moving = NO;
			turning = YES;
			if([currentCompassDirection isEqualToString:@"N"])
				startPositionArray[2] = @"E";
			else if([currentCompassDirection isEqualToString:@"W"])
				startPositionArray[2] = @"N";
			else if([currentCompassDirection isEqualToString:@"S"])
				startPositionArray[2] = @"W";
			else if([currentCompassDirection isEqualToString:@"E"])
				startPositionArray[2] = @"S";
			
		}
		// else
		
		[self updatePosition];
	}
	
	
	return YES;
}

// helper methods
-(void)roomSizeTextfieldIsNotValid{
	roomSizeMessage.hidden = NO;
	// we must set roomsize first and when roomsize is wrong we clear next textfields
	startPositionInput.text = @"";
}
-(void)startPositionTextfieldIsNotValid{
	startPositionInputMessage.hidden = NO;
}

-(void)navigationCommandTextfieldIsNotValid{
	navigationCommandsMessage.text = @"Forkert Input. F L R er korrekt.";
	navigationCommandsMessage.hidden = NO;
}

#pragma mark - Logic. Move some of it to Model class

-(void)setRobotCompassDirection:(NSString*)direction{
	
	NSString * ucDirection = [direction uppercaseString];
	
	if([ucDirection isEqualToString:@"N"])
		robot.transform = CGAffineTransformMakeRotation(-M_PI_4);		// NORTH
	
	else if([ucDirection isEqualToString:@"S"])
		robot.transform = CGAffineTransformMakeRotation(3*M_PI_4);		// SOUTH
	
	else if([ucDirection isEqualToString:@"E"])
		robot.transform = CGAffineTransformMakeRotation(M_PI_4);		// EAST
	
	else if([ucDirection isEqualToString:@"W"])
		robot.transform = CGAffineTransformMakeRotation(-(3*M_PI_4));	// WEST
	else
		robot.transform = CGAffineTransformMakeRotation(0);				// Default
}

- (void)updatePosition {
	
	// tell constraints they need updating
	[self setNeedsUpdateConstraints];
	
	// update constraints now so we can animate the change
	[self updateConstraintsIfNeeded];
	
	[UIView animateWithDuration:0.4 animations:^{
		[self layoutIfNeeded];
	}];
}

// this is Apple's recommended place for adding/updating constraints
- (void)updateConstraints {
	
	[robot mas_remakeConstraints:^(MASConstraintMaker *make) {
		
		make.width.equalTo(@(ROBOT_SIZE));
		make.height.equalTo(@(ROBOT_SIZE));
		
		if(toggleOfOnGrid){
			toggleOfOnGrid = !toggleOfOnGrid;
			[self moveRobot:make];
		}
		
		else if(turning)
		{
			turning = NO;
			[self moveRobot:make];
		}
		
		else if(moving)
		{
			moving = NO;
			[self moveRobot:make];
		}
		
		else if (startPosition)
		{
			startPosition = NO;
			[self offGridOrOnGrid:toggleOfOnGrid];
			[self moveRobot:make];
			
		}
		else // reset
		{
			make.centerX.equalTo(outputViewContainer.mas_centerX);
			make.centerY.equalTo(outputViewContainer.mas_centerY);
		}
	}];
	
	//according to apple super should be called at end of method
	[super updateConstraints];
}

-(void)moveRobot:(MASConstraintMaker*)make
{
	int x = (int) [startPositionArray[0] integerValue];
	int y = (int) [startPositionArray[1] integerValue];
	NSString *compassDirection = startPositionArray[2];
	NSLog(@"x : %d,\t y : %d,\t compass : %@", x, y, compassDirection);
	[self setRobotCompassDirection:compassDirection];
	
	make.centerX.equalTo(outputViewContainer.mas_centerX).with.offset(x*CELL_SIZE-offGridToBeInsideCell_x);
	make.centerY.equalTo(outputViewContainer.mas_centerY).with.offset(y*CELL_SIZE-offGridToBeInsideCell_y);
}

#pragma mark draw grid

-(void)drawGrid{
	
	int columns = (int) [roomSizeArray[0] integerValue];
	int rows = (int) [roomSizeArray[1] integerValue];
	
	UIView * gridContainerView = [UIView new];
	[outputViewContainer addSubview:gridContainerView];
	[gridContainerView mas_remakeConstraints:^(MASConstraintMaker *make) {
		make.width.equalTo(@(columns*CELL_SIZE));
		make.height.equalTo(@(rows*CELL_SIZE));
		make.centerX.equalTo(outputViewContainer.mas_centerX);
		make.centerY.equalTo(outputViewContainer.mas_centerY);
	}];
	
	for (int row = 0; row < rows; row++) {
		for (int column = 0; column < columns; column++) {
			UIView *square = [[UIView alloc] initWithFrame:CGRectMake(column*CELL_SIZE, row * CELL_SIZE, CELL_SIZE, CELL_SIZE)];
			square.layer.borderColor = [UIColor lightGrayColor].CGColor;
			square.layer.borderWidth = 1;
			[gridContainerView addSubview:square];
		}
	}
	
	// create 0,0
	int off_x = 0, off_y = 0;
	if(columns % 2 != 0) off_x = 10;
	if(rows % 2 != 0) off_y = 10;
	UIView * zerozero = [[UIView alloc] initWithFrame:CGRectMake(outputViewContainer.frame.size.width / 2 - 3 - off_x, outputViewContainer.frame.size.height / 2 - 3 - off_y, 5, 5)];
	zerozero.backgroundColor = [UIColor orangeColor];
	[outputViewContainer addSubview:zerozero];
}


@end
