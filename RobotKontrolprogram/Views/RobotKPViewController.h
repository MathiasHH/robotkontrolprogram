//
//  RobotKPViewController.h
//  RobotKontrolprogram
//
//  Created by Mathias Hansen on 30/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RobotKPViewController : UIViewController

- (instancetype)initWithTitle:(NSString *)title viewClass:(Class)viewClass;

@end
