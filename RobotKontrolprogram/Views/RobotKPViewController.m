
//
//  RobotKPViewController.m
//  RobotKontrolprogram
//
//  Created by Mathias Hansen on 30/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import "RobotKPViewController.h"

@interface RobotKPViewController ()
@property (nonatomic, strong) Class viewClass;
@end

@implementation RobotKPViewController

- (instancetype)initWithTitle:(NSString *)title viewClass:(Class)viewClass;
{
	self = [super init];
	if (!self) return nil;
	
	self.title = title;
	self.viewClass = viewClass;
	
	return self;
}

-(void)loadView {
	self.view = self.viewClass.new;
	self.view.backgroundColor = [UIColor brownColor];
}

@end
