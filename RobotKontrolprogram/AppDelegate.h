//
//  AppDelegate.h
//  RobotKontrolprogram
//
//  Created by Mathias Hansen on 30/04/16.
//  Copyright © 2016 Mathias Hedemann Hansen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

